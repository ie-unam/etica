---
title: Dictamen de proyectos
---

## Ruta general

**1.** Para saber si el Comité de Ética del Instituto de Ecología debe
   evaluar y aprobar su proyecto de investigación, responda el
   [cuestionario inicial](../docs/cuestionario_inicial_etica_ie.docx).




**2.** Si respondió que **no** a todas las preguntas, no requiere del viesto bueno del Comité de Ética. Si la respuesta fue afirmativa para al menos una pregunta del cuestionario, envíe los siguientes documentos al correo <eticaiecol@ecologia.unam.mx>


  - El cuestionario inicial respondido
  - Una síntesis del proyecto que especifique los objetivos de investigación y el protocolo de trabajo (máximo 3 cuartillas)
  - [Cuestionario para trabajos con plantas o animales](../docs/plantas_o_animales_etica_ie.docx) 
  - o [formato de carta de consentimiento informado para participantes]() para trabajos con personas. Consulte nuestra [guía para escribir una carta de consentimiento informado](../docs/guia_consentimiento_informado.pdf).



**3.** Su documentación será evaluada por el Comité de Ética a partir de las recomendaciones de dos revisores con experiencia en el área de trabajo.




**4.** En un plazo aproximado de tres semanas, se le enviará un dictamen elaborado por el Comité de Ética del Instituto de Ecología, con o sin recomendaciones.



Los proyectos en curso podrán ser evaluados únicamente hasta junio del 2023. Después de esa fecha sólo se revisarán proyectos que no hayan iniciado.


![Infografía](../images/dictamen_de_proyectos.jpg)
