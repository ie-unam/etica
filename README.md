# Comité de Ética

Sitio web del comité de ética. <https://ie-unam.gitlab.io/etica/>

## Hacer cambios al sitio web

Todos los textos e imágenes del sitio van bajo el directorio [content](https://gitlab.com/ie-unam/etica/-/tree/main/content). Para hacer cambios hay que ser miembro de este repositorio.


## Apariencia del sitio

Cambiar la apariencia del sitio requiere del dominio de html, [Tailwind](https://tailwindcss.com/docs/customizing-colors), [Jinja](https://pypi.org/project/Jinja2/) y [Pelican](https://docs.getpelican.com/en/latest/content.html).

El directorio **theme** tiene plantillas de Jinja/HTML en el subdirectorio *templates*. Las hojas de estilo se hacen con [tailwind](https://tailwindcss.com/docs/customizing-colors), de modo que los estilos se declaran en las etiquetas mismas del html. La única hoja de estilo establece una configuración base para elementos como <a> o <ul>, de los que no se tiene control pues Pelican los genera desde Markdown.


## Ayuda

Preguntas sobre el mantenimiento a esta página web dirigirlas a <rgarcia@iecologia.unam.mx>


## Contribuciones

Aceptamos contribuciones a nuestro sitio web. Escríbanos a <etica@ecologia.unam.mx>
